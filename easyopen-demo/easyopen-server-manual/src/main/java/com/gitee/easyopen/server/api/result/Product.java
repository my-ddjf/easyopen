package com.gitee.easyopen.server.api.result;

import com.gitee.easyopen.doc.annotation.ApiDocField;

/**
 * @author tanghc
 */
public class Product {
    @ApiDocField(description = "商品名称")
    private String name;

//    @ApiDocField(description = "附加商品", beanClass = Product.class)
    private Product extProduct;

    @ApiDocField(description = "附加商品")
    private Goods goods;

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Product getExtProduct() {
        return extProduct;
    }

    public void setExtProduct(Product extProduct) {
        this.extProduct = extProduct;
    }
}
